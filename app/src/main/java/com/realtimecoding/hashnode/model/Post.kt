package com.realtimecoding.hashnode.model

import androidx.annotation.DrawableRes

data class Post(
    val id: String,
    val title: String,
    val subtitle: String,
    val url: String,
    val publication: Publication? = null,
    val metadata: Metadata,
    val paragraphs: List<Paragraph> = emptyList(),
    @DrawableRes val image: Int,
    @DrawableRes val imageThumb: Int
)

data class Metadata(
    val author: PostAuthor,
    val date: String,
    val readTimeMinutes: Int,
    val likes: Int,
    val comments: Int
)

data class PostAuthor(
    val name: String,
    val position: String,
    val url: String? = null,
    @DrawableRes val image: Int,
)

data class Publication(
    val name: String,
    val logoUrl: String
)

data class Paragraph(
    val type: ParagraphType,
    val text: String,
    val markups: List<Markup> = emptyList()
)

data class Markup(
    val type: MarkupType,
    val start: Int,
    val end: Int,
    val href: String? = null
)

enum class MarkupType {
    Link,
    Code,
    Italic,
    Bold,
}

enum class ParagraphType {
    Title,
    Caption,
    Header,
    Subhead,
    Text,
    CodeBlock,
    Quote,
    Bullet,
}
