package com.realtimecoding.hashnode

import android.app.Application
import com.realtimecoding.hashnode.data.AppContainer
import com.realtimecoding.hashnode.data.AppContainerImpl

class HasnodeApplication: Application() {

    // AppContainer instance used by the rest of classes to obtain dependencies
    lateinit var container: AppContainer

    override fun onCreate() {
        super.onCreate()
        container = AppContainerImpl(this)
    }
}