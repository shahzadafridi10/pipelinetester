package com.realtimecoding.hashnode.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.realtimecoding.hashnode.R
import com.realtimecoding.hashnode.model.Post
import com.realtimecoding.hashnode.ui.components.InsetAwareTopAppBar
import com.realtimecoding.hashnode.ui.state.UiState
import kotlinx.coroutines.launch


val TAG: String = "HomeScreen"

@Composable
fun HomeScreen(
    posts: UiState<List<Post>>,
    favorites: Set<String>,
    onToggleFavorite: (String) -> Unit,
    onRefresh: () -> Unit,
    onErrorDismiss: () -> Unit,
    onNavigateToPostDetail: (String) -> Unit,
    scaffoldState: ScaffoldState = rememberScaffoldState()
) {
    val coroutineScope = rememberCoroutineScope()

    if (posts.hasError) {
        val errorMessage = stringResource(id = R.string.load_error)
        val retryMessage = stringResource(id = R.string.retry)

        val onRefreshState by rememberUpdatedState(onRefresh)
        val onErrorDismissState by rememberUpdatedState(onErrorDismiss)

        LaunchedEffect(scaffoldState.snackbarHostState) {
            val snackbarResult = scaffoldState.snackbarHostState.showSnackbar(
                errorMessage,
                retryMessage
            )
            when (snackbarResult) {
                SnackbarResult.ActionPerformed -> onRefreshState()
                SnackbarResult.Dismissed -> onErrorDismissState()
            }
        }
    }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            val title = stringResource(id = R.string.home_title)
            InsetAwareTopAppBar(
                title = { Text(text = title, style = MaterialTheme.typography.h6) },
                navigationIcon = {
                    IconButton(onClick = { coroutineScope.launch { } }) {
                        Icon(
                            painter = painterResource(R.drawable.hasnode_logo),
                            contentDescription = null,
                            modifier = Modifier.size(24.dp,24.dp)
                        )
                    }
                }
            )
        }
    ) { innerPadding ->
        val modifier = Modifier.padding(innerPadding)
        LoadingContent(
            empty = posts.initialLoad,
            emptyContent = { FullScreenLoading() },
            loading = posts.loading,
            onRefreshPosts = onRefresh,
            content = {
                NewsFeed(
                    posts = posts,
                    favorites = favorites,
                    onRefresh = { onRefresh() },
                    navigateToPostDetail = onNavigateToPostDetail,
                    onToggleFavorite = onToggleFavorite,
                    modifier = modifier
                )
            }
        )

    }

}

@Composable
private fun LoadingContent(
    empty: Boolean,
    emptyContent: @Composable () -> Unit,
    loading: Boolean,
    onRefreshPosts: () -> Unit,
    content: @Composable () -> Unit,
) {
    if (empty) {
        emptyContent()
    } else {
        SwipeRefresh(
            state = rememberSwipeRefreshState(loading),
            onRefresh = onRefreshPosts,
            content = content
        )
    }
}

/**
 * Full screen circular progress indicator
 */
@Composable
private fun FullScreenLoading() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.Center)
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun NewsFeed(
    posts: UiState<List<Post>>,
    favorites: Set<String>,
    onRefresh: () -> Unit,
    navigateToPostDetail: (String) -> Unit,
    onToggleFavorite: (String) -> Unit,
    modifier: Modifier
) {
    if (posts.data != null){
        PostCardList(
            posts = posts.data,
            favorites = favorites,
            navigateToPostDetail = navigateToPostDetail,
            onToggleFavorite = onToggleFavorite
        )
    }else if (!posts.hasError) {
        TextButton(onClick = onRefresh, modifier.fillMaxSize()) {
            Text(
                stringResource(id = R.string.home_tap_to_load_content),
                textAlign = TextAlign.Center
            )
        }
    }else{
        Box(modifier.fillMaxSize()) { /* empty screen */ }
    }
}