package com.realtimecoding.hashnode.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.onClick
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.unit.dp
import com.google.accompanist.insets.LocalWindowInsets
import com.google.accompanist.insets.rememberInsetsPaddingValues
import com.realtimecoding.hashnode.R
import com.realtimecoding.hashnode.model.Post

@Composable
fun PostCardList(
    posts: List<Post>,
    favorites: Set<String>,
    navigateToPostDetail: (String) -> Unit,
    onToggleFavorite: (String) -> Unit,
){
    LazyColumn(
        contentPadding = rememberInsetsPaddingValues(
            insets = LocalWindowInsets.current.systemBars,
            applyTop = false
        )
    ){
        posts.forEach { post ->
            item {
                PostCard(
                    post = post,
                    isFavorite = favorites.contains(post.id),
                    navigateToPostDetail = navigateToPostDetail,
                    onToggleFavorite = { onToggleFavorite(post.id) }
                )
                PostListDivider()
            }
        }

    }
}

@Composable
fun PostCard(
    post: Post,
    isFavorite: Boolean,
    navigateToPostDetail: (String) -> Unit,
    onToggleFavorite: (String) -> Unit,
) {
    Card(
        modifier = Modifier
            .background(
                color = Color.Gray,
                shape = RoundedCornerShape(6.dp)
            )
            .padding(20.dp)
    ) {
        Column {

            PostHeader(post = post)

            Spacer(modifier = Modifier.height(19.dp))

            PostContent(post = post)

            Spacer(modifier = Modifier.height(19.dp))

            PostFooter(
                post =post,
                isFavorite = isFavorite,
                onToggleFavorite = { onToggleFavorite(post.id) }
            )
        }
    }
}

@Composable
fun PostHeader(post: Post){
    Row{
        Image(
            painter = painterResource(id = post.metadata.author.image),
            contentDescription = null,
            modifier = Modifier
                .size(40.dp, 40.dp)
                .clip(MaterialTheme.shapes.small)
        )
        Column(
            modifier = Modifier.padding(horizontal = 14.dp)
        ){
            Text(
                text = post.metadata.author.name,
                style = MaterialTheme.typography.subtitle2
            )
            Text(
                text =  post.metadata.author.position,
                style = MaterialTheme.typography.caption
            )
        }

    }
}

@Composable
fun PostContent(post: Post){
    Text(
        text = post.title,
        style = MaterialTheme.typography.subtitle1
    )

    Text(
        text = post.subtitle,
        style = MaterialTheme.typography.body2,
        modifier = Modifier.padding(top = 17.dp),
    )

    Image(
        painter = painterResource(id = post.image),
        contentDescription = null,
        modifier = Modifier
            .height(190.dp)
            .padding(top = 20.dp)
            .fillMaxWidth()
            .clip(MaterialTheme.shapes.small),
        contentScale = ContentScale.Crop
    )
}

@Composable
fun PostFooter(
    post: Post,
    isFavorite: Boolean,
    onToggleFavorite: () -> Unit,
){
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ){
        Row(
            verticalAlignment = Alignment.CenterVertically
        ){

            val clickLabel = stringResource(
                if (isFavorite) R.string.unbookmark else R.string.bookmark
            )

            IconToggleButton(
                checked = isFavorite,
                onCheckedChange = { onToggleFavorite() },
                modifier = Modifier.semantics {
                    this.onClick(label = clickLabel, action = null)
                }
            ) {
                Icon(
                    imageVector = if (isFavorite) Icons.Filled.Favorite else Icons.Filled.FavoriteBorder,
                    contentDescription = null
                )
            }

            Text(
                text = post.metadata.likes.toString(),
                style = MaterialTheme.typography.caption,
                modifier = Modifier.padding(start = 5.dp),
            )

            Spacer(modifier = Modifier.padding(start = 22.dp))

            Icon(
                imageVector = Icons.Filled.ChatBubble,
                contentDescription = null
            )

            Text(
                text = post.metadata.comments.toString(),
                style = MaterialTheme.typography.caption,
                modifier = Modifier.padding(start = 5.dp)
            )

        }

        Icon(
            imageVector = Icons.Filled.Share,
            contentDescription = null
        )
    }
}


/**
 * Full-width divider with padding for [PostList]
 */
@Composable
private fun PostListDivider() {
    Divider(
        modifier = Modifier.padding(horizontal = 14.dp),
        color = MaterialTheme.colors.onSurface.copy(alpha = 0.08f)
    )
}