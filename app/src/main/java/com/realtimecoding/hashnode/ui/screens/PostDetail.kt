package com.realtimecoding.hashnode.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ChatBubble
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material.icons.filled.Share
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.onClick
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.*
import com.realtimecoding.hashnode.R
import com.realtimecoding.hashnode.data.posts.impl.post1
import com.realtimecoding.hashnode.model.Post

@Preview
@Composable
fun PreviewPostDetail(){
    PostDetail(post = post1)
}

@Composable
fun PostDetail(post: Post) {

    Box{
        Surface(
            color = Color.Black.copy(alpha = 0.5f)
        ){
            Image(
                painter = painterResource(id = post.image),
                contentDescription = null,
                modifier = Modifier
                    .height(300.dp)
                    .fillMaxWidth()
                    .clip(MaterialTheme.shapes.small),
                contentScale = ContentScale.FillHeight,
                alpha = 0.7f
            )
        }
        
        Column{

            Spacer(modifier = Modifier.height(130.dp))

            Text(
                text = post.title,
                style = MaterialTheme.typography.h4,
                color = Color.White,
                modifier = Modifier.padding(
                    horizontal = 20.dp
                )
            )

            Spacer(modifier = Modifier.height(8.dp))

            Text(
                text = stringResource(
                    id = R.string.article_post_min_read,
                    formatArgs = arrayOf(
                        post.metadata.date,
                        post.metadata.readTimeMinutes
                    )
                ),
                style = MaterialTheme.typography.button,
                color = Color.White,
                modifier = Modifier.padding(
                    horizontal = 20.dp
                )
            )

            Spacer(modifier = Modifier.height(20.dp))

            PostDetailContent(post = post)

            Spacer(Modifier.height(48.dp))
        }
        
    }
}

@Composable
fun PostDetailContent(post: Post) {

    Box(
        modifier = Modifier
            .background(
                shape = RoundedCornerShape(
                    topStart = 14.dp,
                    topEnd = 14.dp
                ),
                color = Color.White
            )
            .padding(20.dp)
            .fillMaxHeight()
    ) {
        Column {

            Row {
                Image(
                    painter = painterResource(id = post.metadata.author.image),
                    contentDescription = null,
                    modifier = Modifier
                        .size(40.dp, 40.dp)
                        .clip(MaterialTheme.shapes.small)
                )
                Column(
                    modifier = Modifier.padding(horizontal = 14.dp)
                ) {
                    Text(
                        text = post.metadata.author.name,
                        style = MaterialTheme.typography.subtitle2
                    )
                    Text(
                        text = post.metadata.author.position,
                        style = MaterialTheme.typography.caption
                    )
                }

            }

            Text(
                text = post.subtitle,
                style = MaterialTheme.typography.body2,
                modifier = Modifier.padding(top = 17.dp),
                lineHeight = 20.sp
            )

            post.paragraphs.forEach {
                LazyColumn {
                    item {
                        Paragraph(paragraph = it)
                    }
                }
            }
        }
    }

}
