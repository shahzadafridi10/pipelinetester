package com.realtimecoding.hashnode

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.realtimecoding.hashnode.data.AppContainer
import com.realtimecoding.hashnode.ui.theme.HashNodeTheme
import com.realtimecoding.util.produceUiState
import kotlinx.coroutines.launch
import com.realtimecoding.hashnode.ui.screens.HomeScreen as HomeScreen

class MainActivity : ComponentActivity() {
    
    companion object{
        lateinit var appContainer: AppContainer
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appContainer = (application as HasnodeApplication).container
        setContent {
            HashNodeTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    HomeTab()
                }
            }
        }
    }
}

@Preview(showBackground = false)
@Composable
fun HomeTab() {

    val (postUiState, refresh, clearError) = produceUiState(MainActivity.appContainer.postsRepository) {
        getPosts()
    }

    val favorites by MainActivity.appContainer.postsRepository.observeFavorites().collectAsState(setOf())

    val coroutineScope = rememberCoroutineScope()

    HomeScreen(
        posts = postUiState.value,
        favorites = favorites,
        onToggleFavorite = {
            coroutineScope.launch { MainActivity.appContainer.postsRepository.toggleFavorite(it) }
        },
        onRefresh = refresh,
        onErrorDismiss = clearError,
        onNavigateToPostDetail = {}
    )
}
