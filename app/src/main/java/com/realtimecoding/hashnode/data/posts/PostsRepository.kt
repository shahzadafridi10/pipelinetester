package com.realtimecoding.hashnode.data.posts

import com.realtimecoding.hashnode.model.Post
import kotlinx.coroutines.flow.Flow
import com.realtimecoding.hashnode.data.Result

/**
 * Interface to the Posts data layer.
 */
interface PostsRepository {

    /**
     * Get a specific JetNews post.
     */
    suspend fun getPost(postId: String): Result<Post>

    /**
     * Get JetNews posts.
     */
    suspend fun getPosts(): Result<List<Post>>

    /**
     * Observe the current favorites
     */
    fun observeFavorites(): Flow<Set<String>>

    /**
     * Toggle a postId to be a favorite or not.
     */
    suspend fun toggleFavorite(postId: String)

}
